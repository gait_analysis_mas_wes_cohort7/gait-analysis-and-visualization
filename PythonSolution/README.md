# README #

### What is this repository for? ###
* Version 0.0.1

The python solution streams up to 4 IMU's gyroscope and accelerometer data into a database.

### How do I get set up? ###

Summary of set up: 
* Follow mbient's website's Python's "getting started" tutorial: https://mbientlab.com/tutorials/WPython.html.  
Install Microsoft Visual Studio, creating a new project with all .py modules held within the 'Python_Main' directory
or by opening the Microsoft Visual Studio project directly: 'Python_Main.pyproj'
Press Attach/Execute and follow the command prompt's messages to run the program.

* All other folders are standalone, helper simulation folders, not necessary for the Main program.
The "Collected_Data" folder may be of interest as it holds example streamed data and processed stream data for a healthy and an unsymmetrical gait. 
