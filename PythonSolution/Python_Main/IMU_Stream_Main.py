"""Inertial Measurement Unit Streaming

Mbient API C++: https://mbientlab.com/documents/metawear/cpp/0/index.html

To Use now: "python IMU_Stream_Main.py <MAC1> <MAC2>..." and then follow the command prompt messages.  
's' for start stream. 's' for stop stream.

4/26/2019 Status: 9/10 times, collects 188-90 samples for accel_fS=50Hz and gyro_fS=50Hz;
1/10 times, timing seems to be off and either collects small amount of samples.
For sure a bad BLE connection/disconnection from last time b/c after small sample received,
next run is "Failed to enable notifications" (error before even connected to IMU),
then next run is "RuntimeError" that occurs on line d.connect().  Then error clears"

5/10/2019 Status: Next steps:
1. Need to confirm database schema and figure out how to implement the relational parts too
2. Stream to DB table is working
"""

from mbientlab.metawear import MetaWear, libmetawear, parse_value
from mbientlab.metawear.cbindings import *
from time import sleep, time
from threading import Event #used by mbient
#from threading import Thread #added for I/O
import multiprocessing as mp

import platform
import sys

from datetime import datetime
import pytz
from tabulate import tabulate
from gait_databaseORM import * #my import of gait schema (3 tables. 1 for overall gait session info, 2nd for accel raw data, 3rd for gyro raw data.

import numpy as np

def listen_for_stop_stream_command(start_stream_time, a_q, g_q):
    command = ''
    while command != 's':
        command=raw_input("Type 's' and press 'enter' to stop the streaming of motion data from the Inertial Measurement Units (IMUs): \n\t")
    elapsed_stream_time = time() - start_stream_time
    a_q.put((999,999,999,999,999,999,999,999)) #STOP STREAM TUPLE-MESSAGE
    g_q.put((999,999,999,999,999,999,999,999)) #STOP STREAM TUPLE-MESSAGE
    #print "s has been pressed!  \n\tSTOPPED STREAM.\n The stream time was", elapsed_stream_time, "seconds\n"
    print "\tSTOPPED STREAM\n"
    #expected_samples = elapsed_stream_time*50
    #print "\nExpecting up to", round(expected_samples, 3), "samples for each IMU sensor (", round(elapsed_stream_time,3), "s", "*50 Hz)" 
    return elapsed_stream_time

def listen_for_start_stream_command():
    command = ''
    while command != 's':
        command=raw_input("\nType 's' and press 'enter' to start the streaming of motion data from the Inertial Measurement Units (IMUs): \n\t")
    #print "s has been pressed!\n\t  STREAMING NOW\n"
    print "\tSTREAMING NOW\n"

def gait_analysis_process(accel_data_queue, gyro_data_queue, devices_queue):
    """This is a NEW process: will read from the Queue, and get queue data whenever empty, and then do the following:
    - commit the data to the database every 1s
    - filtering, autocorr., etc. """
    DB_type = "file"
    my_session = engine_start_and_open_transaction(DB_type)
    print "\nConnected to the SQLite database via other Process"
    retrieved_id = get_PK_from_info_table(DB_type, my_session)
    print "Retrieved the Primary Key value (unique gait id, 'ID') to be used for this session's database entries \n\t",\
       "ID is",retrieved_id, "\n\n"
    stream = True
    curr_stream_time = time()
    #fS = 50
    #gyro_list = []
    #gyro_list_x = []
    #gyro_list_y = []
    #gyro_list_z = []
    #device_list = []
    #while devices_queue.empty() is False:
    #    device_list.append(devices_queue.get())
    while stream is True:
        Table,id,sensor_mac,sensor_loc,data_time, accel_raw_x,accel_raw_y,accel_raw_z=accel_data_queue.get()
        if Table != 999:
            add_row_into_table(GaitRawAccel_Table,my_session,retrieved_id,sensor_mac,sensor_loc,data_time, accel_raw_x,accel_raw_y,accel_raw_z)
            #Do more stuff here (autocorr, only on waist data)
            #if sensor_loc == "Waist":
                #Then do autocorrelation, o/w it is N/A!
        else:
            stream = False
        Table,id,sensor_mac,sensor_loc,data_time,gyro_raw_x,gyro_raw_y,gyro_raw_z=gyro_data_queue.get()
        if Table != 999:

            #gyro_list_x.append(gyro_raw_x)
            #gyro_list_y.append(gyro_raw_y)
            #gyro_list_z.append(gyro_raw_z)
            #new_data_point = GaitRawGyro_Table()
            #ROM_integral_result = (np.trapz(gyro_list_x), np.trapz(gyro_list_y), np.trapz(gyro_list_z)) 
            #ROM_integral_result = (max(gyro_list_x), max(gyro_list_y), max(gyro_list_z)) 
            #print "ROM Integral Result (x,y,z degrees): ", ROM_integral_result
            #if len(gyro_list_x) > fS:
            #        gyro_list_x = []
            #        gyro_list_y = []
            #        gyro_list_z = []
            #new_data_point.gait_analytics = ROM_integral_result
            #my_session.add(new_data_point)

            add_row_into_table(GaitRawGyro_Table,my_session,retrieved_id,sensor_mac,sensor_loc,data_time,gyro_raw_x,gyro_raw_y,gyro_raw_z)
            #Do more stuff here (integrate)
            #gyro_list.append((sensor_mac, sensor_loc, gyro_raw_z)) 
        else:
            stream = False
        elapsed_stream_time = time() - curr_stream_time #update stream time
        if elapsed_stream_time > 0.5: #This check is equivalent to database update rate.  0.5 sec update worked well (stop stream is immediate)
            my_session.commit()
            curr_stream_time = time() #Reset current stream time to calc. next database update time
            #Do more stuff here: will add query and update here if need to do it in real time
    my_session.commit() #Finally, commit the remaining

    #ALL GAIT DATA ANALYSIS HERE FOR NOW

    gyro_rows_this_gait = my_session.query(GaitRawGyro_Table).get( retrieved_id) #get is only good for primary key
    #print "Getting all with this Primary Key:/n/t", get_fcn_obj_query
    gyro_rows_this_gait.gait_analytics = 999
    my_session.commit()
    
    #update_table (GaitSessions_Table, my_session, retrieved_id, "ROM_avg", total_stream_time)

    #print "This is the gyro list: ", gyro_list
    #print "This is the device list:", device_list
    sys.stdout.flush() 
    my_session.close()


class State:
    """This is mbient's example code with some modifications; each State = IMU object.
    Added to the constructor to hold the 'accel_data_queue', 'gyro_data_queue', 
    and modified the methods to handle the incoming data, wirting to queues that are shared by multiprocesses"""
    def __init__(self, device, (a_q, g_q, s_q)):
        """IMU Object constructor"""
        self.device = device
        self.accel_samples = 0
        self.gyro_samples = 0
        self.accel_callback = FnVoid_VoidP_DataP(self.accel_data_handler) #callback1 for accel
        self.gyro_callback = FnVoid_VoidP_DataP(self.gyro_data_handler) #callback2 for gyro
        self.accel_data_queue = a_q
        self.gyro_data_queue = g_q
        self.all_devices_list = s_q
        #self.accel_analyzed_queue = a_analyzed_q
        #self.gyro_analyzed_queue = g_analyzed_q

        self.location = "NA" #waist, thigh, shin, foot

    def accel_data_handler(self, ctx, data):
        """Accel Callback Fcn: Writes data components into sqlite database & keeps sample count which is printed at end of program"""
        #global my_session (no need for global keyword b/c NOT changing the value; would have need it if so)
        t_now_ms = (datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3])
        accel_into_DB = (GaitRawAccel_Table,"Delete_this_elem", 0, self.device.address, self.location, t_now_ms[11:23],\
           (parse_value(data)).x, (parse_value(data)).y, (parse_value(data)).z)
        e0,e1,e2,e3,e4,e5,e6,e7,e8 = accel_into_DB #parse tuple
        self.accel_samples+= 1
        f.write("%s %s %s -> g's: %s\n" % (self.accel_samples, t_now_ms, self.device.address, parse_value(data))) #accel data, writes <mac>, x, y, &z
        self.accel_data_queue.put((e0,e2,e3,e4,e5,e6,e7,e8))
        #self.accel_analyzed_queue.put((e0,e2,e3,e4,e5,e6,e7,e8)) #2nd copy of queue to be used for data analysis

    def gyro_data_handler(self, ctx, data):
        """Gyro Callback Fcn: Writes data components into sqlite database & keeps sample count which is printed at end of program"""
        t_now_ms = (datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3])
        gyro_into_DB = (GaitRawGyro_Table,"Delete_this_elem", 0, self.device.address,self.location, t_now_ms[11:23],\
           (parse_value(data)).x, (parse_value(data)).y, (parse_value(data)).z)
        e0,e1,e2,e3,e4,e5,e6,e7,e8 = gyro_into_DB #parse tuple
        self.gyro_samples+= 1
        f.write("%s %s %s -> dps: %s\n" % (self.gyro_samples, t_now_ms, self.device.address, parse_value(data))) #gyro data, writes <mac>, x, y, &z
        self.gyro_data_queue.put((e0,e2,e3,e4,e5,e6,e7,e8))
        #self.gyro_analyzed_queue.put((e0,e2,e3,e4,e5,e6,e7,e8)) #2nd copy of queue to be used for data analysis

#class GUI_buttons:
#    def __init__ (self):
#        self.stream_button = "Start_Enabled"
    
#    def toggle_stream_button(self):
#        if self.stream_button == "Start_Enabled":
#            self.stream_button = "Paused_Enabled"
#        else:
#            self.stream_button = "Start_Enabled"
#        print "Stream Button state is =", self.stream_button, "\n"
#    #def __stream_button_status__(self):
#    #    print "Stream Button state is =", stream_button, "\n"

def accel_settings_and_config(s, fS, sensitivity): 
    """args: pointer to IMU object, float, float"""
    print("\tWriting accelerometers configurations")
    libmetawear.mbl_mw_acc_set_odr(s.device.board, 50.0) #OutputDataRate (Hz)
    libmetawear.mbl_mw_acc_set_range(s.device.board, sensitivity) #Range: sensitivity (g)
    libmetawear.mbl_mw_acc_write_acceleration_config(s.device.board)

def gyro_settings_and_config(s, fS, sensitivity): 
    """args: pointer to IMU object, enum, enum
    From c.bindings: class GyroBmi160Odr:_25Hz = 6, _50Hz = 7, _100Hz = 8, ..._3200Hz = 13
    class GyroBmi160Range:_2000dps = 0, _1000dps = 1, _500dps = 2, _250dps = 3, _125dps = 4"""
    print("\tWriting gyroscope configurations")
    libmetawear.mbl_mw_gyro_bmi160_set_odr(s.device.board, fS) #args: ptr, enum
    libmetawear.mbl_mw_gyro_bmi160_set_range(s.device.board, sensitivity) #args: ptr, enum
    libmetawear.mbl_mw_gyro_bmi160_write_config(s.device.board)


def main(): #Main Thread
    #Listing all the globals used
    global user_name 
    global sensor_loc_str
    global f

    if sys.version_info[0] == 2:
        range = xrange
        
    user_name = raw_input("Type your name : ") 
    walk_session_descrp = raw_input("Type a brief description of this gait session, or press 'enter' if none : ") 
    #for i in range(len(sys.argv) - 1):
    #    #WILL NEED TO UPDATE THIS READ IN TO ACTUALLY DIFFERENTIATE BETWEEN DIFF IMU'S WHEN STORING DATA TO DB
    #    #THUS BELOW IS PLACEHOLDER ONLY, NOT DONE IMPLEMENTING FOR MORE THAN 1 IMU APP
    #     sensor_loc_str = raw_input("Enter the sensor location of IMU Module", sys.argv[i+1],": ") 
    sensor_loc_str = "Foot" #Waist, Foot, or Shin

    print "\tHi", user_name, "! Everything will be splendid =]\n"
    f = open("Archive/tempfilesave.txt", "w")

    DB_type = "file"
    #DB_type = "memory"
    my_session_for_info = engine_start_and_open_transaction(DB_type)
    g_fS = 50
    a_fS = 50
    t_now_date_and_min = (datetime.utcnow().strftime('%Y-%m-%d %H:%M'))
    add_row_into_info_table(my_session_for_info, "will autofill", walk_session_descrp , user_name, t_now_date_and_min, a_fS, g_fS)
    my_session_for_info.commit()
    my_session_for_info.close()

    accel_data_queue = mp.Queue() #multiprocessing queue here, tuples as elements
    gyro_data_queue = mp.Queue() #multiprocessing queue here, tuples as elements
    devices_queue = mp.Queue()
    for i in range(len(sys.argv) - 1):
        devices_queue.put(sys.argv[i+1])
    #accel_analyzed_queue = mp.Queue() #multiprocessing queue here, tuples as elements
    #gyro_analyzed_queue = mp.Queue() #multiprocessing queue here, tuples as elements
    data_queues_tuple = (accel_data_queue, gyro_data_queue,devices_queue)
    data_process = mp.Process(target=gait_analysis_process, args=data_queues_tuple)
    data_process.start()

    
    states = []
    IMU_location = []
    for i in range(len(sys.argv) - 1):
        BLE_retries = 5
        BLE_success = False
        #d = MetaWear('EE:66:D4:CA:04:9B')
        #print sys.argv[i+1]
        d = MetaWear(sys.argv[i+1]) #Enter MAC into the "Script Arguments" spot, under VS's Properties-->Debug
        while BLE_retries > 0 and not BLE_success:
            try:
                d.connect()
                print("Bluetooth Low Energy (BLE) host is CONNECTED to " + d.address)
                #states.append(State(d))
                states.append(State(d, data_queues_tuple))
                input_loc = raw_input("\tChoose from the menu this device's location (Waist, R/L Foot, or R/L Shin): ")
                IMU_location.append(input_loc)
                BLE_success=True
            except:
                BLE_retries -= 1
                print "\nBLE connection retries left: ", BLE_retries 
                sleep(0.001) #10ms
   
    print "\n"           
    i=0
    for s in states:
        #print "Battery State ",s.device.address, repr(libmetawear.mbl_mw_settings_get_battery_state_data_signal(s.device.board))
        s.location=IMU_location[i]
        i=i+1
        print "Configuring device", s.device.address
        libmetawear.mbl_mw_settings_set_connection_parameters(s.device.board, 7.5, 7.5, 0, 6000)
        sleep(1.5)
    
        accel_settings_and_config(s, 50.0, 4.0) #IMU object, sample rate, sensitivity (g)
        gyro_settings_and_config(s, GyroBmi160Odr._50Hz, GyroBmi160Range._500dps) #args: IMU object, enum, enum

    listen_for_start_stream_command()

    for s in states:
        accel_signal = libmetawear.mbl_mw_acc_get_acceleration_data_signal(s.device.board)
        libmetawear.mbl_mw_datasignal_subscribe(accel_signal, None, s.accel_callback)
        gyro_signal = libmetawear.mbl_mw_gyro_bmi160_get_rotation_data_signal(s.device.board) #GYRO ADDED
        libmetawear.mbl_mw_datasignal_subscribe(gyro_signal, None, s.gyro_callback)#GYRO ADDED
       
        libmetawear.mbl_mw_acc_enable_acceleration_sampling(s.device.board)
        libmetawear.mbl_mw_gyro_bmi160_enable_rotation_sampling(s.device.board) #GYRO ADDED

        libmetawear.mbl_mw_acc_start(s.device.board)
        libmetawear.mbl_mw_gyro_bmi160_start(s.device.board)#GYRO ADDED

    start_stream_time=time()
    #sleep(30.0) #Stream time
    total_stream_time = listen_for_stop_stream_command(start_stream_time, accel_data_queue, gyro_data_queue) #Main thread is used for the GUI interface

    for s in states:
        #using time() #done during debug -->seems that the stop and disable was the long poll, so separating it so IMU's stop at same time
        libmetawear.mbl_mw_acc_stop(s.device.board)
        libmetawear.mbl_mw_acc_disable_acceleration_sampling(s.device.board)
        libmetawear.mbl_mw_gyro_bmi160_stop(s.device.board) #GYRO ADDED
        libmetawear.mbl_mw_gyro_bmi160_disable_rotation_sampling(s.device.board) #GYRO ADDED

    data_process.join() #This is wait for process to finish before moving on ("joins back to main")
    for s in states:
        accel_signal = libmetawear.mbl_mw_acc_get_acceleration_data_signal(s.device.board)
        libmetawear.mbl_mw_datasignal_unsubscribe(accel_signal)

        gyro_signal = libmetawear.mbl_mw_gyro_bmi160_get_rotation_data_signal(s.device.board) #GYRO ADDED
        libmetawear.mbl_mw_datasignal_unsubscribe(gyro_signal) #GYRO ADDED
    
        libmetawear.mbl_mw_debug_disconnect(s.device.board)
        print("BLE disconnected from " + s.device.address)
        sleep(1.5)
    
    expected_samples = total_stream_time*50
    print "\n\nExpecting up to", round(expected_samples, 3), "samples for each IMU sensor (", round(total_stream_time,3), "s", "*50 Hz)" 
    print "Total Samples Received via IMU Threads"
    accel_tot_samples=0
    gyro_tot_samples=0
    for s in states:
        print(" %s -> %d accelerometer xyz samples" % (s.device.address, s.accel_samples))
        print(" %s -> %d gyroscope xyz samples" % (s.device.address, s.gyro_samples))

    #Opening a new session, may be concurrent with other processes' session
    my_session_for_final_updates = engine_start_and_open_transaction(DB_type)
    PK_num =  get_PK_from_info_table(DB_type, my_session_for_final_updates)
    update_table (GaitSessions_Table, my_session_for_final_updates, PK_num, "totaltime", total_stream_time)
    
    print("\n\nThe SQLite database has been queried for all entries.  \nPress enter to print each table (3 total tables).")
    demo_command = raw_input("")
    print "\tACRONYMS: Stride Symmetry (ratio) = SS, Cadence (steps per minute) = C, Range of Motion (degrees) = ROM"
    query_and_print_whole_table(GaitSessions_Table, my_session_for_final_updates)
    demo_command = raw_input("")
    query_and_print_whole_table(GaitRawAccel_Table, my_session_for_final_updates)
    demo_command = raw_input("")
    query_and_print_whole_table(GaitRawGyro_Table, my_session_for_final_updates)
    my_session_for_final_updates.close()

    #print "\nWrote <date-time-stamp>, <mac>, <accel>, and <gyro> in temporary text file,", f.name, ",for debug purposes only."
    f.close()
    print("\nBye for now!")

if __name__=="__main__":
    main()
    