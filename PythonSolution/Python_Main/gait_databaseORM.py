"""4/29/19 New Module
This module is to be used in integration
Using Object Relational Modeling (ORM), Python's SQLAlchemy

Functions to use should include at least these steps for a complete database transaction: 
1. my_session = engine_start_and_open_transaction()
2. add_row_into_table(GaitRawData_Table,my_session, 0, "timestamp!", "Waist", 99, 99, 99)
   my_session.commit()
3. my_session.close()
4. query_and_print_whole_table(GaitRawData_Table, my_session)
"""

from sqlalchemy import create_engine, Column, Integer, String, Float, ForeignKey
from sqlalchemy.ext.declarative import declarative_base #defines class map to DB
from sqlalchemy.orm import sessionmaker, relationship #for session factory
from tabulate import tabulate

from time import sleep, time #for testing main only

Base = declarative_base() #will provide table definitions for the DB

class GaitSessions_Table(Base): #Inherit "Base"
   # __tablename__ = "gait sessions info table" #lower case is convention 
    __tablename__ = "GAIT SESSIONS INFO TABLE" #lower case is convention 

    id = Column('ID', Integer, primary_key = True) #PK means unique, id=Primary Key
    session_descrp = Column('Session Descrp.', String, nullable = True)
    user = Column("User", String, nullable=False)
    date = Column("Date & Time (UTC)", String, nullable=False)
    totaltime = Column('Length (s)', Float, nullable = True) #Fill in later
    stride_symm_avg =  Column('SS Avg', String, nullable = True) #Fill in later: Stride Symmetry=SS
    cadence_avg =  Column('C Avg (spm)', String, nullable = True) #Fill in later: Cadence=C
    ROM_avg =  Column('ROM Avg (d)', String, nullable = True) #Fill in later: Range of Motion=ROM
    gait_metrics_avg = Column("Overall Analysis", String, nullable = True) #Fill in later

class GaitRawAccel_Table(Base): #Inherit "Base"
    #__tablename__ = "gait raw data table 1: accel x y and z" #lower case is convention
    __tablename__ = "RAW ACCELEROMETER DATA"
    #id = Column('ID', Integer, ForeignKey("gait sessions info table.ID"), nullable = False) #id=Foreign Key
    id = Column('ID', Integer, ForeignKey("GAIT SESSIONS INFO TABLE.ID"), nullable = False) #id=Foreign Key
    accel_PK = Column('i', Integer, primary_key = True) 
    sensor_mac = Column("IMU MAC Address", String, nullable=False)
    sensor_loc = Column("Location", String, nullable=False)
    data_time = Column("Timestamp (UTC)", String, nullable=False)
    accel_x = Column('Accel-X (g)', Float, nullable = False)
    accel_y = Column('Accel-Y (g)', Float, nullable = False)
    accel_z = Column('Accel-Z (g)', Float, nullable = False)
    #gait_analytics = Column('Stride Symmetry', String, nullable = True)
    gait_analytics = Column('SS', String, nullable = True)
    gait_analytics_SS_comment= Column('SS Analysis', String, nullable = True)
    gait_analytics_C = Column('C', String, nullable = True)
    gait_analytics_C_comment = Column('C (spm) Analysis', String, nullable = True)

class GaitRawGyro_Table(Base): #Inherit "Base"
    #__tablename__ = "gait raw data table 2: gyro x y and z" #lower case is convention
    __tablename__ = "RAW GYROSCOPE DATA"
    #id = Column('ID', Integer, ForeignKey("gait sessions info table.ID"), nullable = False) #id=Foreign Key
    id = Column('ID', Integer, ForeignKey("GAIT SESSIONS INFO TABLE.ID"), nullable = False) #id=Foreign Key
    gyro_PK = Column('i', Integer, primary_key = True)
    sensor_mac = Column("IMU MAC Address", String, nullable=False)    
    sensor_loc = Column("Location", String, nullable=False)  
    data_time = Column("Data Time (UTC)", String, nullable=False)
    gyro_x = Column('Gyro-X (dps)', Float, nullable = True) 
    gyro_y = Column('Gyro-Y (dps)', Float, nullable = True) 
    gyro_z = Column('Gyro-Z (dps)', Float, nullable = True) 
    #gait_analytics= Column('Range of Motion (d)', String, nullable = True)
    gait_analytics= Column('ROM (d)', String, nullable = True)
    gait_analytics_ROM_comment= Column('ROM Analysis', String, nullable = True)

def engine_start_and_open_transaction(db_type):
    """Summary: Sets up the database sqlite engine and then creates and opens
    a database 'session factory' that is bound to that newly setup engine.
    REMEMBER: ALWAYS close the session once all done (whenver using this fcn)
    
    Parameters:
        db_type (str) = valid values are 'memory' or 'file' to define how sqlite database will be stored
   
    Returns:
        none"""

    if db_type =="memory":
        #my_engine = create_engine('sqlite:///:memory:', echo=True)
        my_engine = create_engine('sqlite:///:memory:')
    elif db_type =="file":
        my_engine = create_engine('sqlite:///gait_data_demo.db')
        #my_engine = create_engine('sqlite:///gait_data_TESTONLY_DEL.db')
        #my_engine = create_engine('sqlite:///gait_data_OFFICIAL.db')
    else:
        print "Incorrect sqlite type\n"
    Base.metadata.create_all(bind=my_engine) 
    mySession = sessionmaker(bind=my_engine) #open a Transaction
    my_session = mySession()
    return my_session

def query_and_print_whole_table(Table, my_session):
    """Prints entire table to screen"""
    db_data_headers = Table.__table__.columns.keys()
    
    #print "\nHeaders of", Table, "\n\t"
    #print tabulate(() , headers=db_data_headers, tablefmt="fancy_grid")
    print "\n Table", Table
    print "\n\tTITLE: ", Table.__tablename__
    data=[()]    
    if Table == GaitSessions_Table:
        users = my_session.query(GaitSessions_Table).all() #query(Table_Name).all() returns all objects from the Table
        for user in users:
            data_temp=[(user.id, user.session_descrp, user.user, user.date, user.totaltime,\
                  user.stride_symm_avg, user.cadence_avg, user.ROM_avg, user.gait_metrics_avg)]
            data=data+data_temp
        #print "\t\tSS = Stride Symmetry (ratio), C = Cadence (steps per minute), ROM = Range of Motion (degrees)"
        #print "\t SS = Stride Symmetry = ratio of vertical force between the two legs"
        #print "\t C = Cadence = steps per minute"
        #print "\t ROM = Range of Motion = max degrees of rotation per second"
        print"\t"
        print tabulate(data , headers=db_data_headers, tablefmt="github")
    elif Table == GaitRawAccel_Table:
            users = my_session.query(GaitRawAccel_Table).all() #query(Table_Name).all() returns all objects from the Table
            for user in users:
                data_temp=[(user.id,user.accel_PK, user.sensor_mac,user.sensor_loc,user.data_time,\
                   user.accel_x, user.accel_y, user.accel_z, user.gait_analytics, user.gait_analytics_SS_comment,\
                   user.gait_analytics_C, user.gait_analytics_C_comment)]
                data=data+data_temp
            print "\t"
            print tabulate(data , headers=db_data_headers, tablefmt="github") #github yaya nice format

    elif Table == GaitRawGyro_Table:  
            users = my_session.query(GaitRawGyro_Table).all() #query(Table_Name).all() returns all objects from the Table
            for user in users:
                data_temp=[(user.id,user.gyro_PK, user.sensor_mac,user.sensor_loc, user.data_time,\
                   user.gyro_x, user.gyro_y,user.gyro_z, user.gait_analytics, user.gait_analytics_ROM_comment)]
                data=data+data_temp
            print "\t"
            print tabulate(data , headers=db_data_headers, tablefmt="github") #github yaya nice format
    else:
        print "\nINVALID Table Object passed into query_and_print_whole_table(--,--)!\n"

def get_PK_from_info_table(db_type, my_session):
    if db_type =="memory":
        return 1 #PK will be 1
    elif db_type =="file":
        user = my_session.query(GaitSessions_Table).order_by(GaitSessions_Table.id.desc()).first() #This puts id in descending order and queries, so gets last
        return (user.id)
    else:
        return 0
        print "Incorrect sqlite type passed into get_PK_from_info_table fcn\n"
         
def add_row_into_info_table(my_session, id, walk_session_descrp, user, date, gyro_fS, accel_fS):
    """Summary: Adds row to GaitSessions_Table (only first 5 args used right now).  Warning: this 
    does NOT do the commit.  Must perform commit separately for it to make it into the database"""
    new_data_row = GaitSessions_Table()
    #new_data_row.id = id #   id will be autogenerated duh
    new_data_row.session_descrp = walk_session_descrp
    new_data_row.user = user
    new_data_row.date = date

    my_session.add(new_data_row)
    
def add_row_into_table(Table, my_session, id,sensor_mac, sensor_loc,data_time, data_x, data_y,\
                          data_z): #either accel or gyro x y and z
    """Summary: Adds row to either the GaitRawAccel_Table or the GaitRawGyro_Table.  Warning: this 
    does NOT do the commit.  Must perform commit separately for it to make it into the database"""

    if Table == GaitRawAccel_Table:
        new_data_row = GaitRawAccel_Table()
        new_data_row.id = id #Query Session Info table to get this value, and put in here for FK
        new_data_row.sensor_mac  = sensor_mac
        new_data_row.sensor_loc  = sensor_loc #   THIS SHOULD BE INPUT FROM USER
        new_data_row.data_time = data_time #   THIS SHOULD BE INPUT FROM SENSOR DATA        
        new_data_row.accel_x = data_x
        new_data_row.accel_y = data_y
        new_data_row.accel_z = data_z
    elif Table == GaitRawGyro_Table:
        new_data_row = GaitRawGyro_Table()
        new_data_row.id = id #Query Session Info table to get this value, and put in here for FK
        new_data_row.sensor_mac  = sensor_mac
        new_data_row.sensor_loc  = sensor_loc #   THIS SHOULD BE INPUT FROM USER
        new_data_row.data_time = data_time #   THIS SHOULD BE INPUT FROM SENSOR DATA        
        new_data_row.gyro_x = data_x
        new_data_row.gyro_y = data_y
        new_data_row.gyro_z = data_z
    else:
        print "\nINVALID Table Object passed into add_row_into_table!\n"
    my_session.add(new_data_row)

def update_table (Table, my_session, PK_num, col, update_value):
    """Summary: Modifies element in a Table AND commits the change to the database
    Args: 'PK_num' is used as the unique ID to what row the query should get"""
    get_fcn_obj_query = my_session.query(Table).get(PK_num)
    #print "Getting all with this Primary Key:/n/t", get_fcn_obj_query
    if col=="gait_metrics_avg":
        get_fcn_obj_query.gait_metrics_avg = update_value
    elif col=="totaltime":
        get_fcn_obj_query.totaltime = update_value
    else:
        print "\nINVALID column name given to update table fcn"
    my_session.commit()
    
def main():
    """Summary: TEST BENCH CODE FOR THIS MODULE"""
    #engine: create and connect to the sqlite database
    db_type = "memory"
    db_type = "file"
    my_session = engine_start_and_open_transaction(db_type)

    g_fS = 50
    a_fS = 50

    add_row_into_info_table(my_session, "will autofill", "Quick Walk 2", "Amy Chiang", time(), a_fS, g_fS)
    my_session.commit()

    retrieved_id = get_PK_from_info_table(db_type, my_session)
    print "retrieved PK gait id: ", retrieved_id 

    update_table(GaitSessions_Table,my_session, retrieved_id,"gait_metrics_avg", "Heyo!  This is another update")

    add_row_into_table(GaitRawAccel_Table, my_session,  retrieved_id, "MAC", "Waist", "timestamp!", 99,99,99,)
    add_row_into_table(GaitRawGyro_Table, my_session,  retrieved_id, "MAC", "Waist", "timestamp!", 99,99,99,)

    my_session.commit()
    
    query_and_print_whole_table(GaitSessions_Table, my_session)
    query_and_print_whole_table(GaitRawAccel_Table, my_session)
        
    my_session.close()

if __name__ =='__main__': #"__name__" is "__main__" ONLY if run from this file.  #"__name__" is the name of this .py if imported
    main()
