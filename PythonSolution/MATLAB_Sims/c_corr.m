function [ r_xy ] = c_corr( x, ref )
% C_CORR calculated the shiftd cross-correlation
% y or the ref is assumed to be shorter, conj will be taken of y

Lx = length(x);
Ly = length(ref);

[nRows,nCols] = size(x);

tall = 0;
if( nRows > nCols )
    x  = [x; zeros(Ly-1,1)];
    tall = 1;
else
    x = [x zeros(1,Ly-1)];
end

if( tall )
    ref = reshape(ref,Ly,1);
else
    ref = reshape(ref,1,Ly);
end

r_xy = zeros(1,Lx);

for n=1:Lx
    r_xy(n) = sum( x( n + (0:Ly-1) ) .* conj(ref) );
end

r_xy = reshape(r_xy,nRows,nCols);

end
