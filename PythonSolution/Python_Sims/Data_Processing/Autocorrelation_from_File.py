
#import matplotlib.pyplot as plt
import matplotlib.pyplot as plt #import plot, show, title, etc.
from numpy import correlate, arange
import peakutils
#from scipy.signal import find_peaks

import os

os.chdir('C:/Users/achiang/Desktop/UCSD_WES/Capstone/Python/Data')
#os.chdir('C:\Users\achiang\Desktop\UCSD_WES\Capstone\Python\Data')
print "All files in cwd:\n\t", os.listdir(os.getcwd()) #lists all files in the current working dir

#Manual way: close after open duh
    #file = open('Healthy_Gait_Raw_VT_accel.txt', 'r')
    #print "File Name & Mode:\n\t", file.name, file.mode 
    #file.close()

#Old code
    #x = range(0,10) #makes a list that iterates from 1st # and < than 2nd #
    #x= [5, 4, 3, 2, 1]
    #x_autocorred = np.correlate(x,x,'valid')

#Using Context Manager ("Good Practice!"
x = []
fS = 50.0 #50 Hz sampling rate
#with open('Healthy_Gait_Raw_VT_accel.txt', 'r') as file:
#with open('Healthy_Sprint_Raw_VT_accel.txt', 'r') as file:
with open('Long-Short-Long-Short_Gait_Raw_VT_accel.txt', 'r') as file:
    #f_contents = file.read() #loads all into memory and prints
    #f_contents = file.readline() #prints only first line
    #f_contents = file.readlines() #prints all, more compact
    #print f_contents
    cal_out_1g = float(file.readline())
    print "Calibration # is \n\t:", cal_out_1g
    i=0
    x=[]
    for line in file:
        #print float(line) #yes good
        x.append(float(line)-cal_out_1g)

#print "This is x:\n\t", x #yes good

print("Length of original x is %d", len(x))
#x_autocorred=correlate(x,x, 'full') #ALL points
x_autocorred=correlate(x,x, 'same') #Same size as earlier

#print("x is", x)
#print("x with a FULL autocorr is", x_autocorred)

#buffer = 50 #This represents 1 second chunks, used for healthy
#buffer = 258 #for sprint, 130, 192, 258
#begin = len(x_autocorred)/2+buffer
begin = len(x_autocorred)/2+95 #use this for unhealthy gait
end = len(x_autocorred)
x_autocorred_section_normalized = x_autocorred[begin:end]/max(x_autocorred[begin:end])
peak_indices = peakutils.indexes(x_autocorred_section_normalized, thres=0.5, min_dist=8)
print "Peak Indices of the normalized VT autocorr seq section are: ", peak_indices

#Find cadence, using peaks per 3 second window
peak_indices_sec = peak_indices/fS
num_of_seconds = 3.0 #show for only this many seconds
peak_indices_sec_3swindow = []
for i in peak_indices_sec:
    if i <=(num_of_seconds): #+0.1
        peak_indices_sec_3swindow.append(i)
print "Peak Indices of the normalized VT autocorr seq section are, in seconds, within 3 sec: "\
    , peak_indices_sec_3swindow
cadence = (len(peak_indices_sec_3swindow))/num_of_seconds*(60.0/1)/2 #convert to per minute and for 1 foot only
cadence_str = "CADENCE = " + str(round(cadence, 3)) +" "+ "spm"

fig = plt.figure()

#plt.plot(peak_indices[0], x_autocorred_section_normalized[peak_indices[0]], 'r+')
print "y value of autocorr, index=1", x_autocorred_section_normalized[peak_indices[1]]
print "y value of autocorr, index=0", x_autocorred_section_normalized[peak_indices[0]]
SS_value = x_autocorred_section_normalized[peak_indices[1]]/x_autocorred_section_normalized[peak_indices[0]]
print "Stride Symmetery is", SS_value

print "Length of y to plot:", len(x_autocorred_section_normalized)
x_axis_n = arange(len(x_autocorred_section_normalized))
x_axis_t = x_axis_n/fS
print "samples axis:, ", x_axis_n
print "time axis:, ", x_axis_t


ax=fig.gca()
ax.set_xticks(arange(0,4.5,0.5)) 

annotate_color_str = 'black'
ax.annotate("1st Step", (peak_indices[0]/fS, x_autocorred_section_normalized[peak_indices[0]]+0.05),\
    color = annotate_color_str)
ax.annotate("2nd Step", (peak_indices[1]/fS, x_autocorred_section_normalized[peak_indices[1]]+0.05),\
    color = annotate_color_str)
symm_str = "STEP SYMM. = " + str(round(SS_value, 2))
x_annotate_loc = 2.45
ax.annotate("GAIT METRICS PER 3 SEC",(x_annotate_loc,1.05), ha='center', color=annotate_color_str )
#ax.annotate("GAIT METRICS",(x_annotate_loc,1.05), ha='center', color=annotate_color_str )
#ax.annotate("within 3 sec window",(x_annotate_loc,1.1), ha='center', color=annotate_color_str )
ax.annotate(symm_str, (x_annotate_loc, 0.95), ha='center', color=annotate_color_str )
ax.annotate(cadence_str,(x_annotate_loc, 1), ha='center', color=annotate_color_str )
#ax.annotate("3rd Step", (peak_indices[2]/fS, x_autocorred_section_normalized[peak_indices[2]]+0.05))


plt.xlim(0,num_of_seconds)
plt.ylim(-0.2, 1.2)
plt.grid()
plt.plot(x_axis_t, x_autocorred_section_normalized, linewidth=2.7)
for i in peak_indices:
    plt.plot(i/fS, x_autocorred_section_normalized[i], 'rP',markersize=12) #x axis converted to seconds

plt.title('Limping Vertical Autocorrelation Sequence')
#plt.title('Healthy Vertical Autocorrelation Sequence')
#plt.xlabel('sample')
plt.xlabel('seconds')
plt.ylabel('g or m/s^2')
#plot(x_autocorred[600:1200]/max(x_autocorred[600:1200]))
plt.show()
plt.close()
#plt.plot(x_autocorred)
#plt.show()
