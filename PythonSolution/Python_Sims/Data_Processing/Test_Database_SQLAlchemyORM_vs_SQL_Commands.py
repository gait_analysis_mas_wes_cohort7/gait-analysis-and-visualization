import time
import sqlite3
 
 
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
 
 
Base = declarative_base()
session = scoped_session(sessionmaker())
 
 
class User(Base):
    __tablename__ = "user"
    id = Column(Integer, primary_key=True)
    name = Column(String(255))
 
 
def init_db(dbname='sqlite:///example.db'):
    engine = create_engine(dbname, echo=False)
    session.remove()
    session.configure(bind=engine, autoflush=False, expire_on_commit=False)
    Base.metadata.drop_all(engine)
    Base.metadata.create_all(engine)
    return engine
 
 
#def test_sqlalchemy_orm(number_of_records=100000):
def test_sqlalchemy_orm(number_of_records=100):
    init_db()
    start = time.time()
    for i in range(number_of_records):
        user = User()
        user.name = 'NAME ' + str(i)
        session.add(user)
    session.commit()
    end = time.time()
    print "SQLAlchemy ORM: Insert {0} records in {1} seconds".format(
        str(number_of_records), str(end - start)
    )
 
 
#def test_sqlalchemy_core(number_of_records=100000):
def test_sqlalchemy_core(number_of_records=100):
    engine = init_db()
    start = time.time()
    engine.execute(
        User.__table__.insert(),
        [{"name": "NAME " + str(i)} for i in range(number_of_records)]
    )
    end = time.time()
    print "SQLAlchemy Core: Insert {0} records in {1} seconds".format(
        str(number_of_records), str(end - start)
    )
 
def init_sqlite3(dbname="sqlite3.db"):
    conn = sqlite3.connect(dbname)
    cursor = conn.cursor()
    cursor.execute("DROP TABLE IF EXISTS user")
    cursor.execute("CREATE TABLE user (id INTEGER NOT NULL, name VARCHAR(255), PRIMARY KEY(id))")
    conn.commit()
    return conn
 
#def test_sqlite3(number_of_records=100000):
def test_sqlite3(number_of_records=100):
    conn = init_sqlite3()
    cursor = conn.cursor()
    start = time.time()
    for i in range(number_of_records):
        cursor.execute("INSERT INTO user (name) VALUES (?)", ("NAME " + str(i),))
    conn.commit()
    end = time.time()
    print "sqlite3: Insert {0} records in {1} seconds".format(
        str(number_of_records), str(end - start)
    )
 
 
if __name__ == "__main__":
    test_sqlite3()
    test_sqlalchemy_core()
    test_sqlalchemy_orm()
