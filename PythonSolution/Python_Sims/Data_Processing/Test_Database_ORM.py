"""Using Object Relational Modeling (ORM), Python's SQLAlchemy"""

#import sqlalchemy
from sqlalchemy import create_engine, Column, Integer, String, Float, ForeignKey
from sqlalchemy.ext.declarative import declarative_base #defines class map to DB
from sqlalchemy.orm import sessionmaker, relationship #for session factory
from tabulate import tabulate

"""declarative_base
creates a base class from which all mapped class should inherit
Then can use ChildClass.__table__ & ChildClass.__mapper__ """
Base = declarative_base() 

class PersonTable(Base):
    __tablename__ = "person_table" #using lower case as convention for table name

    id = Column('id_headername', Integer, primary_key = True)
    personsname = Column('personsname_headername', String, unique=True)
    totaltime = Column('totaltime_headername', Float, nullable = False)

#echo  = True so it dumps all the SQL, can also run in memory, ':memory:'
#my_engine = create_engine('sqlite:///fooey.db', echo=True)  
my_engine = create_engine('sqlite:///:memory:', echo=True)
#checks for existence of each table, creates if needed.  Also creates FK dependencies
Base.metadata.create_all(bind=my_engine) 


print "Object's keys", PersonTable.__table__.columns.keys()
headers = PersonTable.__table__.columns.keys()

mySession = sessionmaker(bind=my_engine) #open a Transaction
session = mySession()

#Adding data / a user below
    #new_user = PersonTable() #Create instance of Base
    #new_user.id = 0 #fill in attributes
    #new_user.personsname = "Amy"
    #session.add(new_user)
    #session.commit() #must commit transactions for them to happen
users = session.query(PersonTable).all() #Give all objects from PersonTable
for user in users:
    #print user #gives back object
    #Below will give back attributes
    print "User with personsname = %s and id=%d\n\t", user.personsname, user.id
session.close()

mydata = [("Hey", "This is", "3Tuple"), ("Yes", "It'll", "be")]
#headers = ["These", "Are", "The", "Headers"]

print tabulate(mydata, headers=headers, tablefmt="grid")
