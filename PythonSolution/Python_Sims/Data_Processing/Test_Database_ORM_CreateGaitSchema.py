"""This module is intended to be first draft of CreateGaitSchema Module
Using Object Relational Modeling (ORM), Python's SQLAlchemy"""

#import sqlalchemy
from sqlalchemy import create_engine, Column, Integer, String, Float, ForeignKey
from sqlalchemy.ext.declarative import declarative_base #defines class map to DB
from sqlalchemy.orm import sessionmaker, relationship #for session factory
from tabulate import tabulate

"""declarative_base
creates a base class from which all mapped class should inherit
Then can use ChildClass.__table__ & ChildClass.__mapper__ """
Base = declarative_base() 

class GaitSessions_Table(Base): #I believe this is where the schema's are defined...
    __tablename__ = "gait sessions details" #using lower case as convention for table name

    id = Column('ID=PK', Integer, primary_key = True)
    session_descrp = Column('Session Descrp.', String, nullable = True)
    user = Column("User's Name", String, unique=True)
    date = Column("Date & Time", String, nullable=False)
    totaltime = Column('Duration (min)', Float, nullable = False)
    gyro_and_accel_fS = Column("Sensors' fS (Hz)", Float, nullable = False)
    totalsamples = Column('Samples (#)',Integer, nullable = False)
    gait_metrics_avg = Column('Gait Metrics on Average', String, nullable = False)

class GaitRawData_Table(Base): #I believe this is where the schema's are defined...
    __tablename__ = "gait raw data" #using lower case as convention for table name

    #Wouldn't run unless assigned a column in the table as a primary key...hmmmm
    id = Column('ID=FK', Integer, primary_key = True) #WILL MAKE THIS A FOREIGN KEY, NEED TO MAKE RELATIONSHIP LATER
    data_time = Column("Data Timestamp", String, nullable=False)
    sensor_loc = Column("Sensor Location", String, nullable=False)
    accel_x = Column('Accel-X (g)', Float, nullable = False)
    accel_y = Column('Accel-Y (g)', Float, nullable = False)
    accel_z = Column('Accel-Z (g)', Float, nullable = False)
    gyro_x = Column('Gyro-X (dps)', Float, nullable = False)
    gyro_y = Column('Gyro-Y (dps)', Float, nullable = False)
    gyro_z = Column('Gyro-Z (dps)', Float, nullable = False)


#echo  = True so it dumps all the SQL, can also run in memory, ':memory:'
#my_engine = create_engine('sqlite:///fooey.db', echo=True)  
my_engine = create_engine('sqlite:///:memory:', echo=True)
#checks for existence of each table, creates if needed.  Also creates FK dependencies
Base.metadata.create_all(bind=my_engine) 


print "Object's keys of Gait Sessions Table \n\t", GaitSessions_Table.__table__.columns.keys()
print "Object's keys of Gait Raw Data Table \n\t", GaitRawData_Table.__table__.columns.keys()
db_headers = GaitSessions_Table.__table__.columns.keys()
db_data_headers = GaitRawData_Table.__table__.columns.keys()

mySession = sessionmaker(bind=my_engine) #open a Transaction
session = mySession()

#Adding data / a user below
    #new_user = PersonTable() #Create instance of Base
    #new_user.id = 0 #fill in attributes
    #new_user.personsname = "Amy"
    #session.add(new_user)
    #session.commit() #must commit transactions for them to happen
users = session.query(GaitSessions_Table).all() #Give all objects from the Table
for user in users:
    #print user #gives back object (the address)
    #Below will give back attributes
    print "User with personsname = %s and id=%d\n\t", user.personsname, user.id
session.close()

#mydata = [("Hey", "This is", "3Tuple", "dsf", "sdfsdf", "sdfsdf"), ("Yes", "It'll", "be")]
hardcoded_example_data1 = [()]
hardcoded_example_data1 = [(0,"Normal Paced Walk","Amy Chiang","4-26-19 16:01", 5.0, 50.0, 190, "Cadence = 150, Symmetry = 0.8")]
hardcoded_example_data2 = [(1,"Fast Paced Walk","Amy Chiang","4-26-19 16:40", 5.0, 50.0, 190, "Cadence = 190, Symmetry = 0.9")]
hardcoded_example_data = hardcoded_example_data1 + hardcoded_example_data2
#headers = ["These", "Are", "The", "Headers"]
hardcoded_example_gait_raw_data = [(0,"16:01:50:00","Waist",0.02, 0.01, 1.3, 10.4, 26.0, 25.4),\
                                   (0,"16:01:51:05","Waist",0.05, 0.07, 1.3, 8.5, 25.0, 23.4),\
                                   (0,"16:01:51:10","Waist",0.15, 0.12, 0.3, 5.5, 5.0, 20.4)] #a list of tuples
print tabulate(hardcoded_example_data , headers=db_headers, tablefmt="fancy_grid")
print tabulate(hardcoded_example_gait_raw_data, headers=db_data_headers, tablefmt="fancy_grid")
#print tabulate(mydata, headers=headers, tablefmt="grid")
