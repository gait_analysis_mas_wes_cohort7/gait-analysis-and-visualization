from __future__ import print_function
import multiprocessing
import time


def worker( Id ):
    """
    Process worker function.  It receives an Id,
    and prints a message indicating it's starting.
    waits 0.5 seconds, prints another message, and dies.
    """
    print( 'Worker %d started\n' % Id )
    time.sleep( 2.5 )
    print( 'Worker %d finished\n' % Id )
    return

def main():
    """
    Spawns 5 processes that will run the worker function.
    Waits for the threads to finish, then stops.
    """
    jobs = []
    for i in range(5):
        p = multiprocessing.Process( target=worker, args=(i,) )
        jobs.append(p)
        p.start()
        
    print( "Main has spawn all the threads" )

    # wait for all the threads to finish
    for p in jobs:
        p.join()
        
    print( "Done!" )
    
main()
