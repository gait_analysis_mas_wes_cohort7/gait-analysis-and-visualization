# README #

The Gait Visualization and Analysis capstone project is a wearable system of inertial
measurement units (IMU) worn on a human’s lower body to gather gait data which will then be
wirelessly transmitted over Bluetooth Low Energy (BLE) links to a receiver for processing,
analytics, storage, and visualization.  

The solution to gather gait data was achieved in both CSharp and Python.
The CSharpSolution uses Unity as a visual tool for the data and the Python Solution uses prints and plotting modules for the data.

### What is this repository for? ###
* Version 0.0.1
* [Sample link](https://www.google.com)

### How do I get set up? ###

* Summary of set up: see README.md inside "CSharpSolution" folder or "PythonSolution" folder

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact, such as the Professors for UCSD Wireless Embedded Systems, Cohort 7
