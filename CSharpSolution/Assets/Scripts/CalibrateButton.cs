﻿using System.Threading.Tasks;
using UnityEngine.UI;

public class CalibrateButton : SensorButtonBase
{
    bool calibrationInProgress = false;

    void OnGUI()
    {
        GetComponent<Button>().interactable = (sensor != null) && sensor.IsConnected() && sensor.SensorsEnabled && !calibrationInProgress;
    }

    public async void Calibrate()
    {
        await CalibrateAsync();
    }

    public async Task CalibrateAsync()
    {
        if (sensor.IsConnected())
        {
            calibrationInProgress = true;
            await sensor.CalibrateFlatSurfaceAsync();
            calibrationInProgress = false;
        }
    }
}
