﻿using UnityEngine;

public class PulsingLight : MonoBehaviour
{
    public float range = 0.1f;
    public float speed = 0.1f;
    public float minimumRange = 0.05f;

    void Update()
    {
        // Set the x position to loop between 0 and 3
        GetComponentInParent<Light>().range = minimumRange + Mathf.PingPong(Time.time * speed, range - minimumRange);
    }
}
