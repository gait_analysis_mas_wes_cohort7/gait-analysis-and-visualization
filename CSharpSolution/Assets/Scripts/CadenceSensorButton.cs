﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CadenceSensorButton : SensorButtonBase
{
    public void AssignSensorToGraph()
    {
        if (this.sensor != null)
        {
            GameObject.Find("Grapher").GetComponent<GravitationalMovementGraph>().sensor = this.sensor;
        }
    }
}
