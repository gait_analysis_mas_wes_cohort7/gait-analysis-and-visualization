﻿using System.Threading.Tasks;
using UnityEngine.UI;
using UnityEngine;

public class AttachSensorButton : MonoBehaviour
{
    bool attachmentInProgress = false;

    private void OnGUI()
    {
        GetComponent<Button>().interactable = !attachmentInProgress;
    }

    public async void AttachSensor()
    {
        await AttachSensorAsync();
    }

    private async Task AttachSensorAsync()
    {
        attachmentInProgress = true;
        var macAddress = GameObject.Find("InputField").GetComponent<InputField>().text;
        if (!string.IsNullOrEmpty(macAddress))
        {
            var sensor = SensorManager.macAddressToSensorMap[macAddress];
            if (sensor != null)
            {
                await sensor.AttachToGameObjectAsync(transform);
                GameObject.Find("DisplayField").GetComponent<InputField>().text = macAddress;
            }
        }
        attachmentInProgress = false;
        GameObject.Find("InputField").GetComponent<InputField>().text = "";
    }
}
