﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class SensorConfigurationButton : MonoBehaviour
{
    public void LoadSensorConfigurationMenu()
    {
        SceneManager.LoadScene("SensorConfigurationMenu");
    }
}
