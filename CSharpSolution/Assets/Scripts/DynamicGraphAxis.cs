﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DynamicGraphAxis : MonoBehaviour
{
    private GameObject axisX;
    private GameObject axisY;
    // Start is called before the first frame update
    void Start()
    {
        var defaultLength = Camera.main.orthographicSize * 2;

        axisX = GameObject.CreatePrimitive(PrimitiveType.Quad);
        axisX.name = "x-axis";
        axisX.transform.parent = this.transform;
        var pos = Camera.main.transform.position;
        pos.z = 0;
        axisX.transform.position = pos;
        var scale = new Vector3(defaultLength, 0.1f, 1.0f);
        axisX.transform.localScale = scale;

        axisY = GameObject.CreatePrimitive(PrimitiveType.Quad);
        axisY.name = "y-axis";
        axisY.transform.parent = this.transform;
        pos = Camera.main.transform.position;
        pos.x = - Camera.main.orthographicSize;
        pos.z = 0;
        axisY.transform.position = pos;
        scale = new Vector3(0.1f, defaultLength, 1.0f);
        axisY.transform.localScale = scale;
    }
}
