﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SensorUIGroup : MonoBehaviour
{
    public enum ID
    {
        ONE = 1,
        TWO,
        THREE,
        FOUR
    };

    public ID id;

    public GameObject calibrateButtonObject;
    public GameObject connectButtonObject;
    public GameObject deleteButtonObject;
    public GameObject disconnectButtonObject;
    public GameObject startButtonObject;
    public GameObject stopButtonObject;

    public GameObject labelTextObject;

    public GameObject inputFieldObject;

    private void Start()
    {
        // restore saved state
        foreach(var entry in SensorManager.idToSensorMap)
        {
            if (entry.Key == id)
            {
                AssignSensor(entry.Value);
                labelTextObject.GetComponent<Text>().text = entry.Value.MacAddress;
            }
        }
    }

    public void AssignSensor(Sensor sensor)
    {
        calibrateButtonObject.GetComponent<CalibrateButton>().AssignSensor(sensor);
        connectButtonObject.GetComponent<ConnectButton>().AssignSensor(sensor);
        deleteButtonObject.GetComponent<DeleteButton>().AssignSensor(sensor);
        disconnectButtonObject.GetComponent<DisconnectButton>().AssignSensor(sensor);
        startButtonObject.GetComponent<StartButton>().AssignSensor(sensor);
        stopButtonObject.GetComponent<StopButton>().AssignSensor(sensor);
    }
}
