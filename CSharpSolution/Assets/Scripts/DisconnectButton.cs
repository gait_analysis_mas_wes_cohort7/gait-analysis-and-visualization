﻿using System.Threading.Tasks;
using UnityEngine.UI;

public class DisconnectButton : SensorButtonBase
{
    bool disconnectInProgress = false;

    void OnGUI()
    {
        GetComponent<Button>().interactable = (sensor != null) && sensor.IsConnected() && !disconnectInProgress;
    }

    public async void Disconnect()
    {
        await DisconnectAsync();
    }

    public async Task DisconnectAsync()
    {
        if (sensor.IsConnected())
        {
            disconnectInProgress = true;
            await sensor.DisconnectAsync();
            disconnectInProgress = false;
        }
    }
}
