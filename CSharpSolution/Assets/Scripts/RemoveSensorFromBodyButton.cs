﻿using UnityEngine;
using UnityEngine.UI;

public class RemoveSensorFromBodyButton : SensorButtonBase
{
    bool removalInProgress = false;

    private void OnGUI()
    {
        GetComponent<Button>().interactable = !removalInProgress;
    }

    public void RemoveSensorFromBodyPart()
    {
        removalInProgress = true;
        var sensor = SelectableBodyPart.selectedBodyPart.sensor;
        if (sensor != null)
        {
            AssignSensor(null);
            SelectableBodyPart.selectedBodyPart.RemoveSensor(sensor);
            sensor.DetachFromGameObject();
        }
        removalInProgress = false;
        GameObject.Find("InputField").GetComponent<InputField>().text = "";
        SelectableBodyPart.bodyMenu.SetActive(false);
    }
}
