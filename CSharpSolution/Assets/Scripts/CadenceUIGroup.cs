﻿using UnityEngine;
using UnityEngine.UI;

public class CadenceUIGroup : MonoBehaviour
{
    public GameObject sensor1ButtonObject;
    public GameObject sensor2ButtonObject;
    public GameObject sensor3ButtonObject;
    public GameObject sensor4ButtonObject;

    private void Awake()
    {
        var sensorManager = FindObjectOfType<SensorManager>();
        int i = 0;
        foreach(var sensor in SensorManager.macAddressToSensorMap.Values)
        {
            if (i == 0)
            {
                var button = sensor1ButtonObject.GetComponent<CadenceSensorButton>();
                button.AssignSensor(sensor);
                button.GetComponent<Button>().interactable = true;
                button.GetComponent<Button>().GetComponentInChildren<Text>().text = sensor.MacAddress;
            }
            else if (i == 1)
            {
                var button = sensor2ButtonObject.GetComponent<CadenceSensorButton>();
                button.AssignSensor(sensor);
                button.GetComponent<Button>().interactable = true;
                button.GetComponent<Button>().GetComponentInChildren<Text>().text = sensor.MacAddress;
            }
            else if (i == 2)
            {
                var button = sensor3ButtonObject.GetComponent<CadenceSensorButton>();
                button.AssignSensor(sensor);
                button.GetComponent<Button>().interactable = true;
                button.GetComponent<Button>().GetComponentInChildren<Text>().text = sensor.MacAddress;
            }
            else if (i == 3)
            {
                var button = sensor4ButtonObject.GetComponent<CadenceSensorButton>();
                button.AssignSensor(sensor);
                button.GetComponent<Button>().interactable = true;
                button.GetComponent<Button>().GetComponentInChildren<Text>().text = sensor.MacAddress;
            }
            else
            {
                // do nothing
            }
            ++i;
        }
    }
}
