﻿using UnityEngine;

public class SensorButtonBase : MonoBehaviour
{
    //public GameObject sensorObject;
    protected Sensor sensor;

    public void AssignSensor(Sensor sensor)
    {
        this.sensor = sensor;
    }
}
