﻿using System.Collections;
using System.Threading.Tasks;
using UnityEngine.UI;

public class DeleteButton : SensorButtonBase
{
    bool deleteInProgress = false;

    void OnGUI()
    {
        GetComponent<Button>().interactable = (sensor != null) && !deleteInProgress;
    }

    public void Delete()
    {
        if (sensor.IsConnected())
        {
            var sensorUIGroup = GetComponentInParent<SensorUIGroup>();
            deleteInProgress = true;
            SensorManager.Remove(sensorUIGroup.id);
            Destroy(sensor.gameObject);
            sensorUIGroup.AssignSensor(null);
            deleteInProgress = false;
        }
    }
}
