﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class HumanModelButton : MonoBehaviour
{
    public void LoadHumanModelScene()
    {
        SceneManager.LoadScene("HumanModelMenu");
    }
}
