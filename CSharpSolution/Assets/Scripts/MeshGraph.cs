﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshGraph : MonoBehaviour
{
    // Empirically choosen limit that's "big enough"
    public const int MaxMeshVertices = 1 << 13; // 8192

    const MeshTopology meshTopology = MeshTopology.LineStrip;

    // When you use the 'LineStrip' mesh topology you need two indices for the first line.
    // Every line after that only needs 1 new index since it uses the previous index from the previous
    // line. Thus, the 'LineStrip' topolgy makes a connected graph of lines.
    const int MaxIndices = MaxMeshVertices;
    private MeshFilter meshFilter;
    private Mesh mesh;
    private List<Vector3> vertices;
    private List<int> indices;

    private int currentIndex = 1;
    private float currentStep;

    private const float step = 0.2f;
    private float stepLimit;

    //public float NewDataValue { get; set; }
    public float NewDataValue = 0f;

    // Start is called before the first frame update
    void Start()
    {
        // Start the graph at the left edge of the camera
        currentStep = -Camera.main.orthographicSize;

        vertices = new List<Vector3>(MaxMeshVertices) { new Vector3(currentStep, 0, 0) };
        indices = new List<int>(MaxMeshVertices) { 0 };
        stepLimit = Camera.main.orthographicSize;
        meshFilter = GetComponent<MeshFilter>();
        if (meshFilter == null)
        {
            meshFilter = gameObject.AddComponent<MeshFilter>();
        }
        mesh = meshFilter.mesh;
        if (GetComponent<MeshRenderer>() == null)
        {
            gameObject.AddComponent<MeshRenderer>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        UpdateGraph();
    }


    void UpdateGraph()
    {
        if (vertices.Count >= MaxMeshVertices || currentStep >= stepLimit)
        {
            // We don't need to add anymore new vertices. We'll reuse the ones we have.
            // Simulate a scrolling effect by shifting all vertices back one 
            // step on the x-axis and replace the last vertex.
            for (int curr = 0; curr < vertices.Count - 1; ++curr)
            {
                int next = curr + 1;
                vertices[curr] = new Vector3(vertices[curr].x, vertices[next].y, vertices[curr].z);
            }
            vertices[vertices.Count-1] = new Vector3(currentStep, NewDataValue, 0f);
        }
        else
        {
            // Add new vertices and lines to the mesh
            vertices.Add(new Vector3(currentStep, NewDataValue, 0f));
            indices.Add(currentIndex);
            ++currentIndex;
            currentStep += step;
        }
        mesh.vertices = vertices.ToArray();
        // Indices need to be updated after the vertices
        mesh.SetIndices(indices.ToArray(), meshTopology, 0);
    }
}
