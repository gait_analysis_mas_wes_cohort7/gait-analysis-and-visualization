﻿using System.Threading.Tasks;
using UnityEngine.UI;

public class StartButton : SensorButtonBase
{
    bool startInProgress = false;

    private void OnGUI()
    {
        GetComponent<Button>().interactable = (sensor != null) && sensor.IsConnected() && !sensor.SensorsEnabled && !startInProgress;
    }

    public async void StartSensors()
    {
        await StartSensorsAsync();
    }

    public async Task StartSensorsAsync()
    {
        if (sensor.IsConnected())
        {
            startInProgress = true;
            await sensor.StartSensorsAsync();
            startInProgress = false;
        }
    }
}
