﻿using UnityEngine;

/// <summary>
/// Graph Movement along the direction of gravity. Gait Analysis for vertical motion.
/// </summary>
public class GravitationalMovementGraph : MonoBehaviour
{
    public Sensor sensor;
    private MeshGraph graph;
    // Start is called before the first frame update
    void Start()
    {
        sensor = GetComponent<Sensor>();
        graph = GetComponent<MeshGraph>();
        graph.NewDataValue = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if ((sensor == null) || !sensor.IsConnected())
        {
            return;
        }
        // subtract 1 to compensate for gravity (+note the value is in units of g = m/s^2)
        graph.NewDataValue = Vector3.Dot(sensor.Acceleration, sensor.Gravity) - 1;
    }
}
