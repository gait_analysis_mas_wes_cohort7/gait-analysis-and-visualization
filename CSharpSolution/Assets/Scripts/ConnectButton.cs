﻿using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

public class ConnectButton : SensorButtonBase
{
    public GameObject inputFieldObject;
    bool connectionInProgress = false;

    void OnGUI()
    {
        GetComponent<Button>().interactable = (sensor == null) || (!sensor.IsConnected() && !connectionInProgress);
    }

    public async void Connect()
    {
        await ConnectAsync();
    }

    private async Task ConnectAsync()
    {
        var macAddress = inputFieldObject.GetComponent<InputField>().text;
        if (((sensor == null) || !sensor.IsConnected()) && !string.IsNullOrEmpty(macAddress))
        {
            connectionInProgress = true;
            var sensorUIGroup = GetComponentInParent<SensorUIGroup>();
            if (!SensorManager.Contains(macAddress))
            {
                sensor = new GameObject(macAddress).AddComponent<Sensor>();
            }
            DontDestroyOnLoad(sensor);
            if (await sensor.ConnectAsync(macAddress.ToUpper()))
            {
                sensorUIGroup.labelTextObject.GetComponent<Text>().text = sensor.MacAddress;
                sensorUIGroup.AssignSensor(sensor);
                SensorManager.Add(sensorUIGroup.id, sensor);
            }
            connectionInProgress = false;
        }
    }
}
