﻿using UnityEngine;
using UnityEngine.UI;

public class SelectableBodyPart : MonoBehaviour
{
    public enum Id
    {
        None = 0,
        LeftThigh,
        LeftCalf,
        LeftFoot,
        RightThigh,
        RightCalf,
        RightFoot
    };

    public Id id;

    private static Light selectionlight;
    public static GameObject bodyMenu;
    public static SelectableBodyPart selectedBodyPart { get; private set; }

    public Sensor sensor { get; private set; }

    void Awake()
    {
        if (selectionlight == null)
        {
            selectionlight = GameObject.Find("Selection Light").GetComponent<Light>();
        }
        if (bodyMenu == null)
        {
            bodyMenu = GameObject.Find("Body Menu");
            bodyMenu.SetActive(false);
        }
        selectionlight.enabled = false;
    }

    void OnMouseOver()
    {
        var newPosition = transform.position;
        newPosition.z = transform.position.z + -0.1f;
        selectionlight.transform.position = newPosition;

        selectionlight.enabled = true;
    }

    void OnMouseExit()
    {
        selectionlight.enabled = false;
    }

    void OnMouseDown()
    {
        selectedBodyPart = this;
        bodyMenu.SetActive(true);
        if (sensor != null)
        {
            GameObject.Find("DisplayField").GetComponent<InputField>().text = sensor.MacAddress;
        }
        else
        {
            GameObject.Find("DisplayField").GetComponent<InputField>().text = "";
        }
        GameObject.Find("BodyPartField").GetComponent<InputField>().text = id.ToString();
    }

    public bool AddSensor(Sensor sensor)
    {
        if (sensor != null)
        {
            if (sensor.MacAddress == GameObject.Find("DisplayField").GetComponent<InputField>().text)
            {
                return true;
            }
            if (!RemoveSensor(this.sensor))
            {
                Debug.LogError($"Could not remove {sensor.MacAddress} from {this.name}");
                return false;
            }
        }
        SensorManager.macAddressToBodyPart[sensor.MacAddress] = sensor;
        SensorManager.bodyPartToSensorMap[id] = sensor;
        this.sensor = sensor;
        return true;
    }

    public bool RemoveSensor(Sensor sensor)
    {
        if (sensor == null)
        {
            return true;
        }
        this.sensor = null;
        return SensorManager.macAddressToBodyPart.Remove(sensor.MacAddress) && SensorManager.bodyPartToSensorMap.Remove(id);
    }
}
