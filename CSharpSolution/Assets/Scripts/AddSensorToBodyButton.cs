﻿using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

public class AddSensorToBodyButton : SensorButtonBase
{
    bool addInProgress = false;

    private void OnGUI()
    {
        GetComponent<Button>().interactable = !addInProgress;
    }

    public async void AddSensorToBodyPart()
    {
        await AddSensorToBodyPartAsync();
    }

    private async Task AddSensorToBodyPartAsync()
    {
        addInProgress = true;
        var macAddress = GameObject.Find("InputField").GetComponent<InputField>().text;
        if (!string.IsNullOrEmpty(macAddress))
        {
            var sensor = SensorManager.macAddressToSensorMap[macAddress];
            if (sensor != null)
            {
                AssignSensor(sensor);
                SelectableBodyPart.selectedBodyPart.AddSensor(sensor);
                await sensor.AttachToGameObjectAsync(SelectableBodyPart.selectedBodyPart.transform);
            }
        }
        addInProgress = false;
        GameObject.Find("InputField").GetComponent<InputField>().text = "";
        SelectableBodyPart.bodyMenu.SetActive(false);
    }
}
