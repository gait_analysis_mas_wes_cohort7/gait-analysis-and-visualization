﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using MbientLab.MetaWear;

using UnityEngine;

public static class Scanner
{
    public const int DefaultScanTimeMilliseconds = 10000;

    public static void ScanForMetaWears(int timeoutMilliseconds = DefaultScanTimeMilliseconds)
    {
        var devices = new List<MbientLab.Warble.ScanResult>();
        var seen = new HashSet<string>();
        // Set a handler to process scanned devices
        MbientLab.Warble.Scanner.OnResultReceived = item => {
            // Filter devices that do not advertise the MetaWear service or have already been seen
            if (item.HasServiceUuid(Constants.METAWEAR_GATT_SERVICE.ToString()) && !seen.Contains(item.Mac))
            {
                seen.Add(item.Mac);

                Debug.Log($"[{devices.Count}] = {item.Mac} ({item.Name})");
                devices.Add(item);
            }
        };

        MbientLab.Warble.Scanner.Start();
        Task.Delay(timeoutMilliseconds).Wait();
        MbientLab.Warble.Scanner.Stop();
    }

    public static bool ScanForMetaWear(string macAddress, int timeoutMilliseconds = DefaultScanTimeMilliseconds)
    {
        var found = false;
        MbientLab.Warble.Scanner.OnResultReceived = item => {
            // Filter devices that do not advertise the MetaWear and don't match the MAC Address
            if (item.HasServiceUuid(Constants.METAWEAR_GATT_SERVICE.ToString()) && (item.Mac == macAddress))
            {
                Debug.Log($"Found {macAddress}");
                found = true;
            }
        };

        MbientLab.Warble.Scanner.Start();
        var scanTask = Task.Factory.StartNew(() => { while (!found){} }, TaskCreationOptions.LongRunning);
        scanTask.Wait(timeoutMilliseconds);
        MbientLab.Warble.Scanner.Stop();

        // return selected mac address
        return found;
    }
}
