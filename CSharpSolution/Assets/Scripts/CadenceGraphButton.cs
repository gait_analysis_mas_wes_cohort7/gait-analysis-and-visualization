﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class CadenceGraphButton : MonoBehaviour
{
    public void LoadCadenceScene()
    {
        SceneManager.LoadScene("CadenceGraphMenu");
    }
}
