﻿using System.Collections.Generic;
using UnityEngine;

public class SensorManager : MonoBehaviour
{
    // Keeps track of which sensor UI group is associated with which sensor
    public static Dictionary<SensorUIGroup.ID, Sensor> idToSensorMap = new Dictionary<SensorUIGroup.ID, Sensor>();
    // Keeps track of what sensors are in use or have been used
    public static Dictionary<string, Sensor> macAddressToSensorMap = new Dictionary<string, Sensor>();
    // Keeps track of which body part is associated with which sensor
    public static Dictionary<SelectableBodyPart.Id, Sensor> bodyPartToSensorMap = new Dictionary<SelectableBodyPart.Id, Sensor>();
    // Keeps track of which sensor mac address is associated with which body part
    public static Dictionary<string, Sensor> macAddressToBodyPart = new Dictionary<string, Sensor>();

    static HashSet<string> sensorAddresses = new HashSet<string>();
    static readonly object sensorsLock = new object();
    static readonly object sensorAddressesLock = new object();

    void Awake()
    {
        if (FindObjectsOfType<SensorManager>().Length == 1)
        {
            DontDestroyOnLoad(this.gameObject);
        }
    }

    public static bool Contains(string macAddress)
    {
        lock(sensorsLock)
        {
            return macAddressToSensorMap.ContainsKey(macAddress);
        }
    }

    public static bool Add(SensorUIGroup.ID id, Sensor sensor)
    {
        lock(sensorsLock)
        {
            if (!macAddressToSensorMap.ContainsKey(sensor.MacAddress))
            {
                idToSensorMap[id] = sensor;
                macAddressToSensorMap.Add(sensor.MacAddress, sensor);
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    public static bool Remove(SensorUIGroup.ID id)
    {
        lock(sensorsLock)
        {
            if (idToSensorMap.TryGetValue(id, out var sensor))
            {
                return macAddressToSensorMap.Remove(sensor.MacAddress) && idToSensorMap.Remove(id);
            }
            return false;
        }
    }
}
