﻿using System;
using System.Threading.Tasks;
using MbientLab.MetaWear;
using UnityEngine;

public class Sensor : MonoBehaviour
{
    IMetaWearBoard metaWearBoard;

    // Sensor state
    bool disconnectedOnPurpose = false;
    public bool SensorsEnabled { get; private set; } = false;
    public bool AttachedToObject { get; private set; } = false;
    public string MacAddress { get { return metaWearBoard != null ? metaWearBoard.MacAddress : ""; } }

    // Sensor Readings
    public Vector3 Acceleration;
    public Vector3 AngularVelocity;
    public Vector3 Gravity;

    // Calibration parameters
    private bool calibrationEnabled = false;
    public int accCalibrationSampleCount = 0;
    public int gyroCalibrationSampleCount = 0;
    public float accCalibrationX, accCalibrationY, accCalibrationZ;
    public float gyroCalibrationX, gyroCalibrationY, gyroCalibrationZ;

    // IMU to Quaternion parameters
    public float SamplePeriod = 1f / 50f; // 1/Hz = seconds
    public float Beta = 1f;
    public float Ki = 0f;
    public float Kp = 1f;
    private float[] eInt = new float[] { 0f, 0f, 0f };

    // Interaction object Parameters
    private Transform transformToControl;

    void Awake()
    {
        if (transformToControl == null)
        {
            ResetTransformToControl();
        }
    }

    private void ResetTransformToControl()
    {
        transformToControl = GetComponentInParent<Transform>();
    }

    async Task Autostart(string macAddress)
    {
        try
        {
            if (await ConnectAsync(macAddress))
            {
                await StartSensorsAsync();
                Debug.Log($"START Calibrating {metaWearBoard.MacAddress}");
                await CalibrateFlatSurfaceAsync();
                Debug.Log($"STOP Calibrating {metaWearBoard.MacAddress}");
            }
            else
            {
                Debug.LogError($"Start Failed: {macAddress}");
            }
        }
        catch (Exception e)
        {
            Debug.LogException(e);
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (IsConnected() && !calibrationEnabled && SensorsEnabled)
        {
            CalculateQuaternionMahonyDcmFilter();
        }
        if (AttachedToObject)
        {
            //this.transform.parent.transform.localRotation *= this.transform.localRotation;
        }
    }

    async void OnDestroy()
    {
        await DisconnectAsync();
    }

    public async Task<bool> ConnectAsync(string macAddress, int retries = 100)
    {
        if (IsConnected() && (metaWearBoard.MacAddress == macAddress))
        {
            // No need to do anything
            return true;
        }
        else if ((metaWearBoard == null) || (metaWearBoard.MacAddress != macAddress))
        {
            try
            {
                metaWearBoard = MbientLab.MetaWear.NetStandard.Application.GetMetaWearBoard(macAddress);
            }
            catch (Exception)
            {
                Debug.LogError($"Failed to get a MetaWearBoard Instace for ${macAddress}");
                if (metaWearBoard == null)
                {
                    return false;
                }
                else
                {
                    // just continue to use the last valid instance of the MetaWearBoard
                    Debug.LogWarning($"Using last valid MetaWearBoard instance {metaWearBoard.MacAddress}");
                }
            }
            // Setup callback for unexpected disconnects
            metaWearBoard.OnUnexpectedDisconnect = async () =>
            {
                if (disconnectedOnPurpose)
                {
                    return;
                }
                Debug.Log($"Disconnected unexpectedly: Trying to reconnect to {metaWearBoard.MacAddress}");
                if (await ConnectAsync(metaWearBoard.MacAddress, retries))
                {
                    if (SensorsEnabled)
                    {
                        await StartSensorsAsync();
                    }
                }
            };
        }
        metaWearBoard.TimeForResponse = 1000;
        Debug.Log($"Attempting to connect to {macAddress}...");
        do
        {
            try
            {
                await metaWearBoard.InitializeAsync();
                // Reset MetaWearBoard configuration (doesn't do a full firmware reset)
                metaWearBoard.TearDown();
                retries = -1;
            }
            catch (Exception)
            {
                retries--;
            }
        } while (retries > 0);

        if (retries == 0)
        {
            Debug.LogWarning($"Failed to connect to {macAddress}.");
            return false;
        }
        else
        {
            metaWearBoard.GetModule<MbientLab.MetaWear.Core.ISettings>()?.EditBleConnParams(maxConnInterval: 7.5f);
            await Task.Delay(1500);
            Debug.Log($"Connected to {metaWearBoard.MacAddress}");
            return true;
        }
    }

    public async Task DisconnectAsync()
    {
        if (IsConnected())
        {
            StopSensors();
            metaWearBoard.TearDown();
        }
        if (metaWearBoard != null)
        {
            disconnectedOnPurpose = true;
            await metaWearBoard.GetModule<MbientLab.MetaWear.Core.IDebug>().DisconnectAsync();
            Debug.Log($"Disconnected from {this.metaWearBoard.MacAddress}");
        }
        else
        {
            Debug.LogError($"Failed to disconnect from NULL instance of MetaWearBoard.");
        }
    }

    public async Task StartSensorsAsync()
    {
        var accelerometer = metaWearBoard.GetModule<MbientLab.MetaWear.Sensor.IAccelerometerBmi160>();
        Debug.Log($"Starting Accelerometer {metaWearBoard.MacAddress}");
        accelerometer.Configure(
            MbientLab.MetaWear.Sensor.AccelerometerBmi160.OutputDataRate._50Hz,
            MbientLab.MetaWear.Sensor.AccelerometerBosch.DataRange._4g);

        await accelerometer.Acceleration.AddRouteAsync((source) =>
        {
            source.Stream((data) =>
            {
                var acc = data.Value<MbientLab.MetaWear.Data.Acceleration>();
                Acceleration = new Vector3(acc.X - accCalibrationX, acc.Y - accCalibrationY, acc.Z - accCalibrationZ);
                if (calibrationEnabled)
                {
                    ++accCalibrationSampleCount;
                    accCalibrationX += acc.X;
                    accCalibrationY += acc.Y;
                    accCalibrationZ += acc.Z;
                }
            });
        });
        accelerometer.Acceleration.Start();
        accelerometer.Start();

        var gyroscope = metaWearBoard.GetModule<MbientLab.MetaWear.Sensor.IGyroBmi160>();
        Debug.Log($"Starting Gyroscope {metaWearBoard.MacAddress}");
        gyroscope.Configure(
            MbientLab.MetaWear.Sensor.GyroBmi160.OutputDataRate._50Hz,
            MbientLab.MetaWear.Sensor.GyroBmi160.DataRange._1000dps,
            MbientLab.MetaWear.Sensor.GyroBmi160.FilterMode.Osr4);

        await gyroscope.AngularVelocity.AddRouteAsync((source) =>
        {
            source.Stream((data) =>
            {
                var angVel = data.Value<MbientLab.MetaWear.Data.AngularVelocity>();
                AngularVelocity = new Vector3(angVel.X - gyroCalibrationX, angVel.Y - gyroCalibrationY, angVel.Z - gyroCalibrationZ);
                AngularVelocity /= 250; // experimental desensitizing fudge factor. Understand this better. I just guessed that since I'm using a Osr4 filter which effectively reduces the sampling rate by 4 I should divide the output by dps/4 which in this case is 500 dps / 4
                if (calibrationEnabled)
                {
                    ++gyroCalibrationSampleCount;
                    gyroCalibrationX += angVel.X;
                    gyroCalibrationY += angVel.Y;
                    gyroCalibrationZ += angVel.Z;
                }
            });
        });
        gyroscope.AngularVelocity.Start();
        gyroscope.Start();

        SensorsEnabled = true;
    }

    public void StopSensors()
    {
        var accelerometer = metaWearBoard.GetModule<MbientLab.MetaWear.Sensor.IAccelerometerBmi160>();
        Debug.Log($"Stopping Accelerometer {metaWearBoard.MacAddress}");
        accelerometer.Stop();
        accelerometer.Acceleration.Stop();

        var gyroscope = metaWearBoard.GetModule<MbientLab.MetaWear.Sensor.IGyroBmi160>();
        Debug.Log($"Stopping Gyroscope {metaWearBoard.MacAddress}");
        gyroscope.Stop();
        gyroscope.AngularVelocity.Stop();

        SensorsEnabled = false;
    }

    public bool IsConnected()
    {
        return metaWearBoard != null && metaWearBoard.IsConnected;
    }

    public async Task CalibrateFlatSurfaceAsync(int timeInMilliseconds = 5000)
    {
        if (metaWearBoard != null)
        {
            Debug.Log($"Calibrating {metaWearBoard.MacAddress}");
        }
        accCalibrationSampleCount = 0;
        gyroCalibrationSampleCount = 0;

        accCalibrationX = 0;
        accCalibrationY = 0;
        accCalibrationZ = 0;

        gyroCalibrationX = 0;
        gyroCalibrationY = 0;
        gyroCalibrationZ = 0;

        calibrationEnabled = true;
        await Task.Delay(timeInMilliseconds);
        calibrationEnabled = false;

        accCalibrationX /= accCalibrationSampleCount;
        accCalibrationY /= accCalibrationSampleCount;
        accCalibrationZ /= accCalibrationSampleCount;
        accCalibrationZ -= 1;

        gyroCalibrationX /= gyroCalibrationSampleCount;
        gyroCalibrationY /= gyroCalibrationSampleCount;
        gyroCalibrationZ /= gyroCalibrationSampleCount;
    }
 
    public void CalculateQuaternionMahonyDcmFilter()
    {
        Quaternion q = transformToControl.localRotation;

        // Estimated direction of gravity
        Gravity = new Vector3(2.0f * (q.x * q.z - q.w * q.y), 2.0f * (q.w * q.x + q.y * q.z), q.w * q.w - q.x * q.x - q.y * q.y + q.z * q.z);

        // Normalise accelerometer measurement
        Vector3 accNorm = Acceleration.normalized;
        if (accNorm == Vector3.zero) return; // handle NaN

        Vector3 error = Vector3.Cross(accNorm, Gravity);
        if (Ki > 0f)
        {
            eInt[0] += error.x;      // accumulate integral error
            eInt[1] += error.y;
            eInt[2] += error.z;
        }
        else
        {
            eInt[0] = 0.0f;     // prevent integral wind up
            eInt[1] = 0.0f;
            eInt[2] = 0.0f;
        }

        // Apply feedback terms to angular velocity (gyroscope readings)
        var g = new Vector3(
            AngularVelocity.x + Kp * error.x + Ki * eInt[0],
            AngularVelocity.y + Kp * error.y + Ki * eInt[1],
            AngularVelocity.z + Kp * error.z + Ki * eInt[2]);

        // Integrate rate of change of quaternion
        float pa = q.x;
        float pb = q.y;
        float pc = q.z;
        q.w = q.w + (-q.x * g.x - q.y * g.y - q.z * g.z) * (0.5f * SamplePeriod);
        q.x = pa + (q.w * g.x + pb * g.z - pc * g.y) * (0.5f * SamplePeriod);
        q.y = pb + (q.w * g.y - pa * g.z + pc * g.x) * (0.5f * SamplePeriod);
        q.z = pc + (q.w * g.z + pa * g.y - pb * g.x) * (0.5f * SamplePeriod);

        // Update rotation
        transformToControl.localRotation = q.normalized;
    }

    public async Task AttachToGameObjectAsync(Transform transform)
    {
        // TODO this doesn't work as expected. Someone needs to figure out what rotation matrix to appy to the
        // attached object so that it correctly rotates with respect to the sensor readings.
        // Maybe write a different version of CalculateQuaternionMahonyDcmFilter() to take into account
        // the initial Quaternion of the attached object.
        SensorsEnabled = false;
        this.transform.parent = transform;
        //this.transform.position = transform.position;
        //this.transform.rotation = transform.rotation;
        await Task.Delay(100);
        //await CalibrateFlatSurfaceAsync();
        //accCalibrationZ += 1f;
        transformToControl = transform;
        SensorsEnabled = true;
    }

    public void DetachFromGameObject()
    {
        this.transform.parent = null;
        ResetTransformToControl();
        DontDestroyOnLoad(this);
    }

}
