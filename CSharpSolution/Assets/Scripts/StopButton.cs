﻿using UnityEngine;
using UnityEngine.UI;

public class StopButton : SensorButtonBase
{
    bool stopInProgress = false;

    private void OnGUI()
    {
        GetComponent<Button>().interactable = (sensor != null) && sensor.IsConnected() && sensor.SensorsEnabled && !stopInProgress;
    }

    public void StopSensor()
    {
        if (sensor.IsConnected())
        {
            stopInProgress = true;
            sensor.StopSensors();
            stopInProgress = false;
        }
    }
}
