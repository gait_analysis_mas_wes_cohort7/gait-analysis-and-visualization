﻿# README #

This is the C# version of the Gait Analysis software.

### What is this repository for? ###
* Version 0.0.1

### How do I get set up? ###

* Install Unity
* Import the .unitypackage file
* Add scenes to build settings
** File -> Build Settings
** You can either drag scenes into the "Scenes In Build" list or add them one by one

Note: The other files are just for convenience (You can look at the source files without installing Unity)